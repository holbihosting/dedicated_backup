#!/bin/bash

while getopts p: opts; do
   case ${opts} in
      p) DBPASSW=${OPTARG} ;;
   esac
done

TIMESTAMP=`date '+%y_%m_%d_%H'`
BAKDIR="/var/local/backup/dbs"
MONTH=`date '+%d'`
if [ -z $DBPASSW ]; then	
	DBPASSW_OPTION=""
else
	DBPASSW_OPTION="-p$DBPASSW"
fi

mkdir -p $BAKDIR
for i in $(mysql -u root $DBPASSW_OPTION -Bse 'show databases'|grep -v schema); do
        mysqldump --opt -uroot $DBPASSW_OPTION $i > $BAKDIR/$i.$TIMESTAMP.sql
        gzip $BAKDIR/$i.$TIMESTAMP.sql
done
ls -lah $BAKDIR | grep $TIMESTAMP >> /var/local/backup/dbs.log
find $BAKDIR/ -maxdepth 1 -type f -name "*.sql.gz" -mtime +14 -exec rm -f '{}' \;

### if [ "$MONTH" = "01" ]; then
###	mkdir -p $BAKDIR/MONTHLY-$(date '+%B')
###	for i in $(mysql -u root $DBPASSW_OPTION -Bse 'show databases'|grep -v schema); do
###      	mysqldump --opt -uroot $DBPASSW_OPTION $i > $BAKDIR/MONTHLY-$(date '+%B')/$i.$TIMESTAMP.sql
###        	gzip $BAKDIR/$i.$TIMESTAMP.sql
###	done
###fi
