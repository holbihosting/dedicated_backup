#!/bin/bash
BACKUPDIR='/var/local/backup'
PWD="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SCRIPTPATH=$PWD


if [ -f ${SCRIPTPATH}/settings.cfg ]; then

	# include settings with configuration	
	source ${SCRIPTPATH}/settings.cfg

	#check the destination server
	if [[ -n "${BACKUPSERVER}" ]]; then

		if [[ -n "${NODE}" ]]; then
			REALNODE="${NODE,,}/"
                        REALBACKUPSERVER=${BACKUPSERVER}
			#echo 'case 1'

		else
			REALNODE=""
                        REALBACKUPSERVER=${BACKUPSERVER}
			#echo 'case 2'

		fi

	else

		if [[ -n "${NODE}" ]]; then
                        REALNODE="${NODE,,}/"
                        REALBACKUPSERVER="94.23.219.94"
			#echo 'case 3'
		else
                        REALNODE=""
                        REALBACKUPSERVER="94.23.219.94"
			#echo 'case 4'

		fi


	fi
	
	ssh remback@151.80.32.22 "[ ! -d /home/remback/${REALNODE}$(hostname -f) ] && mkdir -p /home/remback/${REALNODE}$(hostname -f)"
	/usr/bin/rsync -e ssh -av --delete ${BACKUPDIR}/ remback@${REALBACKUPSERVER}:/home/remback/${REALNODE}$(hostname -f)/ > ${BACKUPDIR}/sync.log
	#echo "/usr/bin/rsync -e ssh -av --delete ${BACKUPDIR}/ remback@${REALBACKUPSERVER}:/home/remback/${REALNODE}$(hostname -f)/"
	find ${BACKUPDIR}/ -type f -cmin -300 -name '*.gz' -exec ls -lha {} \; >> ${BACKUPDIR}/sync.log
	cat ${BACKUPDIR}/sync.log | mail -s "$(hostname -f) backup report" ${EMAIL}
	echo "" > ${BACKUPDIR}/sync.log
else

	echo "file ${SCRIPTPATH}/settings.cfg does not exists" | mail -s "backup on $(hostname -f) FAILURE" hosting@holbi.co.uk && exit

fi
