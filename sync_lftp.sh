#!/bin/bash

#FTPDIR='/backup'

SCRIPTPATH='/root/bin/HH-backup'


if [ -f ${SCRIPTPATH}/settings.cfg ]; then

# include settings with configuration   
source ${SCRIPTPATH}/settings.cfg


lftp -f "
open $FTPHOST
user $FTPUSER $FTPPASS
lcd $BACKUPDIR
mirror --reverse --delete --continue --use-cache --parallel=2 --verbose $BACKUPDIR $FTPREMOTEDIR
bye
"

else

        echo "file ${SCRIPTPATH}/settings.cfg does not exists" | mail -s "backup on $(hostname -f) FAILURE" hosting@holbi.co.uk && exit

fi

# rsync -e ssh -av --delete $BACKUPDIR/ remback@94.23.219.94:/home/remback/$(hostname -f)/ >> $BACKUPDIR/sync.log
# cat $BACKUPDIR/sync.log | mail -s "$(hostname -f) backup report" hosting@holbi.co.uk
# echo "" > $BACKUPDIR/sync.log

