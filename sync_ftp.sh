#!/bin/bash
BACKUPDIR='/var/local/backup'
PWD="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SCRIPTPATH=$PWD


if [ -f ${SCRIPTPATH}/settings.cfg ]; then

        # include settings with configuration   
        source ${SCRIPTPATH}/settings.cfg

        #check the destination server
        if [[ -n "${BACKUPSERVER}" ]]; then

                if [[ -n "${NODE}" ]]; then
                        REALNODE="${NODE,,}/"
                        REALBACKUPSERVER=${BACKUPSERVER}
                        #echo 'case 1'

                else
                        REALNODE=""
                        REALBACKUPSERVER=${BACKUPSERVER}
                        #echo 'case 2'

                fi

        else

                if [[ -n "${NODE}" ]]; then
                        REALNODE="${NODE,,}/"
                        REALBACKUPSERVER="8.8.8.8"
                        #echo 'case 3'
                else
                        REALNODE=""
                        REALBACKUPSERVER="8.8.8.8"
                        #echo 'case 4'

                fi


        fi

	[ ! -d ${FTPLOCALDIR} ] && mkdir -p ${FTPLOCALDIR}

	function backup_rotating()
	{
	       #Content backup rotation, removes all directories WEEKLY-* older than 7 days, i.e. stores 2 weekly backups for 7 days guarantee backup
        	for old_backups in $(find ${FTPLOCALDIR}/$SITE -type d -mtime +${CONTENT_ROTATION} -name WEEKLY-*); do
                	rm -rf $old_backups
        	done

	        # Database rotation
	        find ${FTPLOCALDIR}/dbs/ -maxdepth 1 -type f -name "*.sql.gz" -mtime +${DATABASE_ROTATION} -exec rm -f '{}' \;


	#       /usr/bin/rsync -e ssh -av --delete ${BACKUPDIR}/ remback@${REALBACKUPSERVER}:/home/remback/${REALNODE}$(hostname -f)/ > ${BACKUPDIR}/sync.log
	#       #echo "/usr/bin/rsync -e ssh -av --delete ${BACKUPDIR}/ remback@${REALBACKUPSERVER}:/home/remback/${REALNODE}$(hostname -f)/"
	}

	#Mount FTP backup directory
        if [ -x "$(command -v curlftpfs)" ]; then
        	curlftpfs ftp://${FTPUSER}:${FTPPASS}@${FTPHOST} ${FTPLOCALDIR}
		if [ $? != 0 ]; then
			umount ${FTPLOCALDIR} && curlftpfs ftp://${FTPUSER}:${FTPPASS}@${FTPHOST} ${FTPLOCALDIR}
			if [ $? != 0 ]; then
				echo "FTP connection is not correct.Please check access details"
				echo "FTP connection is not correct.Please check access details" | mail -s "backup on $(hostname -f) FAILURE" $EMAIL
				exit
			fi
                fi
	
        else
		echo " curlftpfs does not exist. Please install it"
		echo " Ftp client does not exist. Please install it" | mail -s "backup on $(hostname -f) FAILURE" $EMAIL
		exit
        fi
	
	# rotate old backups
	backup_rotating

        # moving content and databases to remote FTP server
	if [[ "${FTPLOCALDIR}" == /var/local/*backup ]] || [[ "${BACKUPDIR}" == /var/local/backup ]] ; then
		cp -r --preserve=timestamps ${BACKUPDIR}/* ${FTPLOCALDIR}/
		if [ $? == 0 ]; then
			find ${BACKUPDIR} -type f -name "*.gz" -mtime -30 -exec rm -rf {} \;
		else
			find ${BACKUPDIR} -type f -name "*.gz" -mtime +7 -exec rm -rf {} \;
			echo "backup is not successfully copied to remote backup server ${FTPHOST}. Please check"
			echo "backup is not successfully copied to remote backup server ${FTPHOST}. Please check" | mail -s "backup on $(hostname -f) FAILURE" $EMAIL
		fi
#                       /usr/bin/rsync -e ssh -av --remove-source-files --progress --temp-dir=/tmp ${BACKUPDIR}/ ${FTPLOCALDIR}/
	else
		echo "FTP folder path is not correct"
		echo "FTP folder path is not correct. Please check" | mail -s "backup on $(hostname -f) FAILURE" $EMAIL
	fi
	

        find ${FTPLOCALDIR}/ -type f -cmin -300 -name '*.gz' -exec ls -lha {} \; >> ${BACKUPDIR}/sync.log
        cat ${BACKUPDIR}/sync.log | mail -s "$(hostname -f) backup report" ${EMAIL}
        echo "" > ${BACKUPDIR}/sync.log

	# unmount FTP
        fusermount -u ${FTPLOCALDIR}
else

        echo "file ${SCRIPTPATH}/settings.cfg does not exists" | mail -s "backup on $(hostname -f) FAILURE" hosting@holbi.co.uk && exit

fi
