#!/bin/bash


# version of client, have to change when code should update
CLVERSION='0.2.171018'

#Content backup rotation,guarantee 7 days of backup (SET IN DAYS!)
#CONTENT_ROTATION="7"
#Database backup rotation, 7 days usually according to Holbi Hosting SLA (SET IN DAYS!)
#DATABASE_ROTATION="7"
#Current directory
PWD="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#Write $BACKUPDIR to file for import
echo $BACKUPDIR > $PWD/backupdir
TIMESTAMP=$(date '+%d-%m-%Y')
#EMAIL="hosting@holbi.co.uk"
CONTENT_LIST=$PWD/content
DATE=`date '+%d.%m.%Y'`
WEEK=`date '+%V'`
DAY=`date '+%u'`
MONTH=`date '+%d'`

#SCRIPTPATH='/root/bin/HH-backup'
SCRIPTPATH=$PWD
echo '' > $CONTENT_LIST
CPAN=$(/bin/netstat -anp | grep ":2087" | awk {'print $4'}| cut -d ":" -f2)

source $SCRIPTPATH/settings.cfg
if [ $? != 0 ]; then
        echo "Configuration file is not persists. Please fix it" | mail -s "backup on $(hostname -f) FAILURE" hosting@holbi.co.uk && exit
fi

while getopts t:m:u:p:h: opts; do
   case ${opts} in
        t) BACKUPDIR=${OPTARG} ;;
        m) METHOD=${OPTARG} ;;
        u) FTP_USER=${OPTARG} ;;
        p) FTP_PASSWORD=${OPTARG} ;;
        h) FTP_HOST=${OPTARG} ;;
   esac
done

# Check for backup directory option
if [[ -z $BACKUPDIR ]]; then
	BACKUPDIR="/var/local/backup"
fi

[ ! -d $BACKUPDIR ] && mkdir -p $BACKUPDIR

# Check for method option
if [[ "$METHOD" == "ftp" ]]; then
	#Check for FTP_USER option
	if [[ -z $FTP_USER ]]; then
		echo "FTP_USER OPTION IS NOT SET when trying to use FTP METHOD"
		echo "FTP_USER OPTION IS NOT SET when trying to use FTP METHOD" | mail -s "backup on $(hostname -f) FAILURE" $EMAIL && exit
	fi
	
	#Check for FTP_PASSWORD option
	if [[ -z $FTP_PASSWORD ]]; then
		echo "FTP_PASSWORD OPTION IS NOT SET when trying to use FTP METHOD"
                echo "FTP_PASSWORD OPTION IS NOT SET when trying to use FTP METHOD" | mail -s "backup on $(hostname -f) FAILURE" $EMAIL && exit
        fi
	
	#Check for FTP_HOST option
	if [[ -z $FTP_HOST ]]; then
		echo "FTP_HOST OPTION IS NOT SET when trying to use FTP METHOD"
                echo "FTP_HOST OPTION IS NOT SET when trying to use FTP METHOD" | mail -s "backup on $(hostname -f) FAILURE" $EMAIL && exit
        fi

	mkdir -p $BACKUPDIR
	BACKUPSERVER='ftp'
	#Mount FTP backup directory
	if [ -x "$(command -v curlftpfs)" ]; then
		curlftpfs ftp://${FTP_USER}:${FTP_PASSWORD}@${FTP_HOST} ${BACKUPDIR}
		if [ $? != 0 ]; then
			umount ${BACKUPDIR} && curlftpfs ftp://${FTP_USER}:${FTP_PASSWORD}@${FTP_HOST} ${BACKUPDIR}
			if [ $? != 0 ]; then
				echo "FTP connection is not correct.Please check access details"
				echo "FTP connection is not correct.Please check access details" | mail -s "backup on $(hostname -f) FAILURE" $EMAIL
				exit
			fi
                fi
	else
	    echo " curlftps does not exist"
	    echo " curlftps does not exist" | mail -s "backup on $(hostname -f) FAILURE" $EMAIL
	    exit
	fi
fi

if [[ "$NFS" == "on" ]]; then
	# check nfs connection
	CHECKNFSONLINE=$(df -h | grep $BACKUPDIR | awk {' print $6'})
	if [[ "${CHECKNFSONLINE}" == "${BACKUPDIR}" ]]; then
		echo 'test' > $BACKUPDIR/testwrite.txt
		if [ $? != 0 ]; then
		echo "We can not write to NFS backup directory. Please check it" | mail -s "backup on $(hostname -f) FAILURE" $EMAIL && exit
		fi
	else
		echo "NFS connection is not persists. Please check it" | mail -s "backup on $(hostname -f) FAILURE" $EMAIL && exit	
	fi
fi

#Find absolute path to public_html directory
#find /home -maxdepth 2 -type d -name "public_html" > $CONTENT_LIST
if [ "$BACKUP_SUBDOMAINS" == 'on' ]; then

	# subdomains case addon for CPanel
	if [[ $CPAN == '2087' ]] ; then
	#	find /home/ -maxdepth 2 -type d -name "subdomains" | while read line; do
	#		find ${line} -maxdepth 1 -type d | grep -v "$line" -x | awk '{print $0"/"}' >> $CONTENT_LIST
	#	done

		# start of CPANEL custom subdomain functionality
		
		cd /var/cpanel/users
		for USER in *; do
		        if [ "$USER" == "system" ] || [ "$USER" == "root" ]; then
		                echo "skip user $USER" > /dev/null
		        else
		        TOTALLINES=`cat /var/cpanel/userdata/$USER/main|wc -l`
		        SUBLINE=`grep -n sub_domains /var/cpanel/userdata/$USER/main | awk -F ":" '{print $1}'`
		        IP=`cat /var/cpanel/users/$USER|grep ^IP|cut -d= -f2`;
		        if [ `echo ${#IP}` -lt 4 ]; then
		                IP=`cat /var/cpanel/mainip`
		        fi
		
		#SUBDOMAIN and ADDONS
		        TOT1=`echo "$TOTALLINES-$SUBLINE" | bc`
		        for SUB in `cat /var/cpanel/userdata/$USER/main | tail -n $TOT1 | awk '{print $2}' | xargs -L100` ; do
		                ROOT=`cat /var/cpanel/userdata/$USER/"$SUB" | grep documentroot | awk '{print $2}'`
		                #echo 'subdomain= ' $SUB
		                echo $ROOT:$SUB >> $CONTENT_LIST
		        done
		#MAIN DOMAIN and his aliases
		        DOMAIN=`grep main_domain /var/cpanel/userdata/$USER/main | awk '{print $2}' `
		        ROOT=`cat /var/cpanel/userdata/$USER/"$DOMAIN" | grep documentroot | awk '{print $2}'`
		        #echo 'domain= ' $DOMAIN
		        echo $ROOT:$DOMAIN >> $CONTENT_LIST
		
		        fi
		done
		
		# end of CPANEL custom subdomain functionality
	else
		# subdomains case for others
		find /home/ -maxdepth 4 -type d -name "public_html" > $CONTENT_LIST
	fi
	#cat $SCRIPTPATH/additinal_backup_list.txt >> $CONTENT_LIST
else
	# without subdomain case
	find /home/ -maxdepth 2 -type d -name "public_html" > $CONTENT_LIST
fi

STATUSFAIL='false'
FAILEDSITE=''
#COUNTER=0
#echo ${COUNTER}
REWRITE='true'
if [[ $VERBOSETAR == 'off' ]]; then
	vbtar=''
else
	vbtar='v'
fi

for CONTENT in $(cat $CONTENT_LIST); do
#	SITE="$(echo $CONTENT | awk -F '/' '{print $3}')"
	if [ "$BACKUP_SUBDOMAINS" == 'on' ]; then
		if [[ $CPAN == '2087' ]] ; then

			# start of CPANEL custom subdomain functionality
			DOMAINCPNAME=$(echo $CONTENT | awk -F ':' '{print $2}')
			#echo "domainname= $DOMAINCPNAME"
			CONTENT=$(echo $CONTENT | awk -F ':' '{print $1}')
			#echo "content= $CONTENT"
			SITE="$(echo $CONTENT | awk -F '/' '{print $3}')"
			SITE="$(echo ${SITE}'_'${DOMAINCPNAME})"
			# end of CPANEL custom subdomain functionality
		else
			# new SITE block for subdomains
			SITELEVEL=$(leveltest="$CONTENT"; grep -o '/'  <<< $leveltest| wc -l)
			#echo $SITELEVEL                                                                                                       
			if [ $SITELEVEL -gt 4 ]; then
				SITE="$(echo $CONTENT | awk -F '/' '{print $3"_"$5}')"
			elif [ ${SITELEVEL} -eq 4 ]; then
				SITE="$(echo $CONTENT|awk -F '/' '{print $3"_"$4"_"$5}')"
			else
				SITE="$(echo $CONTENT | awk -F '/' '{print $3}')"
			fi
			# end of SITE block
		fi
	else
		SITE="$(echo $CONTENT | awk -F '/' '{print $3}')"
	fi
	EXCLUDE_LIST=$BACKUPDIR/exclude.list
	LOG="$SCRIPTPATH/log"
	mkdir -p $BACKUPDIR/$SITE
	if [ ! -f $EXCLUDE_LIST ]; then
		echo -e "*.tar\n*.gz\n*.sql\n*.tgz\n*.zip\n*.bz2\n*.log\n*.txt\n*.xml" > $EXCLUDE_LIST
	fi
	# If it's Monday we're doing a full backup, if not - increment. 
        if [ "$DAY" == 1 ]; then
		if [ "$REWRITE" == 'true' ]; then
                	echo "*** Creating FULL content backup of $SITE ***" > $LOG
			REWRITE='false'
		else
			echo "*** Creating FULL content backup of $SITE ***" >> $LOG
		fi
		#Content backup rotation, removes all directories WEEKLY-* older than 7 days, i.e. stores 2 weekly backups for 7 days guarantee backup
		for old_backups in $(find $BACKUPDIR/$SITE -type d -mtime +${CONTENT_ROTATION} -name WEEKLY-*); do
			rm -rf $old_backups
		done
		[ ! -d $BACKUPDIR/$SITE/WEEKLY-$WEEK ] && mkdir -p $BACKUPDIR/$SITE/WEEKLY-$WEEK
		# Rotation of old meta folders older than 7 days for incremental backups
		for old_metafolders in $(find $SCRIPTPATH/meta/$SITE -type d -mtime +${CONTENT_ROTATION} -name "[0-9][0-9]"); do
                        if [[ "$SCRIPTPATH/meta/$SITE" == /root/*/meta/* ]]; then
				rm -rf $old_metafolders
			else
				echo 'path for old meta folders was incorrect. Please check' | mail -s "$(hostname -f) backup FAILURE!!!" $EMAIL
			fi
                done
		# Create local directory for 'meta' files because it doesn't work on ftp location
		mkdir -p $SCRIPTPATH/meta/$SITE/$WEEK
		# Create variable for 'meta' file
		META=$SCRIPTPATH/meta/$SITE/$WEEK/meta
                /bin/tar -${vbtar}czf $BACKUPDIR/$SITE/WEEKLY-$WEEK/FULL-$SITE.tar.gz --listed-incremental=$META --exclude-from=$EXCLUDE_LIST $CONTENT >> $LOG

		#checking created tarball
                gunzip -c $BACKUPDIR/$SITE/WEEKLY-$WEEK/FULL-$SITE.tar.gz | tar t > /dev/null
		if [ $? != 0 ]; then
			STATUSFAIL='true'
			FAILLOG="$SCRIPTPATH/faillog"
			cat ${LOG} >> ${FAILLOG}
			FAILEDSITE=${SITE}
                fi

                echo "" >> $LOG
        else
                echo "*** Day $DAY. Creating INCREMENTAL content backup of $SITE ***" >> $LOG
		[ ! -d $BACKUPDIR/$SITE/WEEKLY-$WEEK ] && mkdir -p $BACKUPDIR/$SITE/WEEKLY-$WEEK
		#LOCATION=$(ls $BACKUPDIR/$SITE|grep WEEKLY-$WEEK-*) # Finding directory for weekly increment backups
                # Create local directory for 'meta' files because it doesn't work on ftp location
                mkdir -p $SCRIPTPATH/meta/$SITE/$WEEK
                # Create variable for 'meta' file
                META=$SCRIPTPATH/meta/$SITE/$WEEK/meta

                /bin/tar -${vbtar}czf $BACKUPDIR/$SITE/WEEKLY-$WEEK/INC-$SITE-$DAY.tar.gz --listed-incremental=$META --exclude-from=$EXCLUDE_LIST $CONTENT >> $LOG

		# checking created tarball
                gunzip -c $BACKUPDIR/$SITE/WEEKLY-$WEEK/INC-$SITE-$DAY.tar.gz | tar t > /dev/null
                if [ $? != 0 ]; then
			STATUSFAIL='true'
			FAILLOG="${SCRIPTPATH}/faillog"
                        cat ${LOG} >> ${FAILLOG}
			FAILEDSITE=$SITE
                fi

		echo "" >> $LOG
        fi

<<MONTHLY
	# If it's 1st day of month - we're doing a FULL monthly backup
	if [ "$MONTH" = 1 ]; then
		echo "*** Creating MONTHLY FULL content backup if $SITE ***" >> $LOG
		mkdir $BACKUPDIR/$SITE/MONTHLY-$DATE/
		/bin/tar -czvf $BACKUPDIR/$SITE/MONTHLY-$DATE/FULL-$SITE.tar.gz --exclude-from=$EXCLUDE_LIST $CONTENT &>> $LOG
		echo "" >> $LOG
	fi
MONTHLY

#	COUNTER=$((COUNTER + 1))
#	echo "counter = ${COUNTER}"

done

if [[ -z $MYSQL_BACKUP_ENABLE ]] || [[ "$MYSQL_BACKUP_ENABLE" == 'on' ]]; then

	if [ -f $SCRIPTPATH/settings.cfg ]; then
	
	        # Database backup
	        [ ! -d $BACKUPDIR/dbs ] && mkdir -p $BACKUPDIR/dbs
	        if [[ $CPAN == '2087' ]]; then
	                DBPASSW_OPTION=''
	        else
	                DBPASSW_OPTION="-p$MYSQL_PASS"
	        fi
	
	        for i in $(mysql -h $MYSQL_HOST -u $MYSQL_USER $DBPASSW_OPTION -Bse 'show databases'|grep -v schema); do
	                mysqldump --opt -h $MYSQL_HOST -u $MYSQL_USER $DBPASSW_OPTION $i | gzip > $BACKUPDIR/dbs/$i.$TIMESTAMP.sql.gz
	                if [ $? != 0 ]; then
	                        STATUSFAIL='true'
				FAILLOG="${SCRIPTPATH}/faillog"
				echo "db backup for $i was not created" >> ${FAILLOG}
			fi
	        done
	        # Database rotation
	        find $BACKUPDIR/dbs/ -maxdepth 1 -type f -name "*.sql.gz" -mtime +${DATABASE_ROTATION} -exec rm -f '{}' \;
	
	else
	        echo "file $SCRIPTPATH/settings.cfg does not exists" | mail -s "backup on $(hostname -f) FAILURE" $EMAIL && exit
	fi
	
fi

if [[ $STATUSFAIL == 'true' ]]; then
	echo 'start fail processing..'
	if [ -n "${FAILLOG}" ]; then
		cat ${FAILLOG} | mail -s "$(hostname -f) backup FAILURE!!!" $EMAIL
		echo '' > ${FAILLOG}
		echo 'fail is true'
	else
		echo 'some of backup had been failed' | mail -s "$(hostname -f) backup FAILURE!!!" $EMAIL
	fi
else
	if [[ -z ${BACKUPSERVER} ]]; then
		BACKUPSERVER='8.8.8.8'
	fi
	/usr/bin/wget --no-check-certificate -O /dev/null https://holbihost.co.uk/noc/rep/vohnkm.php?hostname=$(hostname -f)'&'cont=${NODE}'&'rserver=${BACKUPSERVER}'&'clversion=${CLVERSION}
fi


#Unmount FTP directory if FTP METHOD is set, send information message
if [[ "$METHOD" == "ftp" ]]; then
	find $BACKUPDIR -type f -ctime -1 -name '*.gz' -exec ls -lha {} \; > $BACKUPDIR/backupresult.log
        cat $BACKUPDIR/backupresult.log | mail -s "$(hostname -f) backup report" $EMAIL

	# unmount FTP
	fusermount -u $BACKUPDIR
fi
