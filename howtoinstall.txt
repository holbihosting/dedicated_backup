Create backup:
---------------------------------
1. upload to the server backup scripts to the folder /root/bin/HH-backup
2. copy and configure file from settings.sample.cfg to settings.cfg (mysql pass to database backup)
3. add to the cronjob the next line:

0 2 * * * /usr/bin/nice -n 10 /root/bin/HH-backup/backup.sh

you can configure time if it need.

4. check the work of /usr/bin/nice , /usr/bin/wget and /usr/bin/mail
5. check the work of script

Synchronization with remote backup server using ssh:
---------------------------------
1. by default backup have to synchronize with main backup server, you it need to another server, please insert variable $BACKUPSERVER to settings.cfg
2. If server is VPS, insert $NODE to settings.cfg with name of subscription of the server
3. create a access with authorized keys to the remote backup server
4. add to the cronjob the next line:

0 3 * * * /usr/bin/nice -n 10 /root/bin/HH-backup/sync.sh

5. check the work of script

Synchronization with remote backup server using ftp and quotas on the backup server:
---------------------------------
1. create FTP user with quotas on the backup server
2. you should insert access details to FTP server to the file settings.cfg
3. add to the cronjob the next line:

0 3 * * * /usr/bin/nice -n 10 /root/bin/HH-backup/sync_ftp.sh

4. check the work of /usr/bin/curlftpfs and install if this program is not exits
5. check the work of script


Synchronization with remote backup server using lftp:
---------------------------------
1. you should insert access details to FTP server to the file settings.cfg
2. insert FTP working catalogs to settings.cfg
3. add to the cronjob the next line:

0 3 * * * /usr/bin/nice -n 10 /root/bin/HH-backup/sync_lftp.sh

4. check the work of /usr/bin/lftp
5. check the work of script


Work of script with separate disks through curlftpfs
---------------------------------
1. upload to the server backup scripts to the folder /root/bin/HH-backup
2. you should insert password to the main user of mysql to the file /root/inf, if it need
3. add to the cronjob the path of script with access details to FTP disks. Folder /home/ftp-backup have to be created. After the script done its job, connection to curlftp will be closed

0 2 * * * /root/bin/HH-backup/backup.sh -t /home/ftp-backup -m ftp -u username -p password -h servername

you can configure time if it need.

4. check the work of /usr/bin/nice , /usr/bin/wget and /usr/bin/mail
5. check the work of script
